import mongoose from "mongoose";

/*
	Wir legen die Struktur unserer User Dokumente fest.
	Wir können über das Modell auf die Datenbank zugreifen.
*/
const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	userName: {
		type: String,
		required: true
	}
});

// dritter Parameter Collection name, siehe auch mongoose connection.db.collection
export default mongoose.model("User", userSchema, "users");