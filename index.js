import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import userRouter from "./routes/userRouter.js";

// get environment variables
dotenv.config();

const app = express();

// HTTP Daten auslesen, url encoded
app.use(bodyParser.urlencoded({ extended: true }));

// Server Config
const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || "http://localhost";

// zu MongoDB Atlas verbinden
mongoose.connect(process.env.MONGODB_URI)
	.then(() => {
		app.listen(PORT, () => {
			console.log(`Server läuft: ${HOST}:${PORT}`);
		});
	})
	.catch(err => console.log(err));

// Alle users routes sind unter /users/... erreichbar, wie in userRouter definiert
app.use("/users", userRouter);
