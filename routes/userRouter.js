import express from "express";
import { all, single, create, update, deleteUser } from "../controller/userController.js";

const router = new express.Router();

router.get("/", all);
router.get("/:id", single);
router.post("/", create);
router.put("/:id", update);
router.delete("/:id", deleteUser)

export default router;