/*
	Controller beinhaltet alle Callbacks der User routes
*/
import User from "../model/userModel.js";

/**
 * Alle User anzeigen
 * @param Object req
 * @param Object res
 */
export const all = async (req, res) => {
	res.send("Show all users");
};

/**
 * Einzelnen User anhand seiner Id anzeigen
 * @param Object req
 * @param Object res
 */
export const single = async (req, res) => {
	const id = req.params.id;
	res.send(`Show a single user: ${id}`);
};

/**
 * Einen neuen User anlegen
 * Die benötigten Felder sind im userModel definiert.
 * @param Object req
 * @param Object res
 */
export const create = async (req, res) => {
	try {
		const userData = req.body;
		// neuen User mit Hilfe des Usermodels anlegen
		const newUser = new User(userData);

		// Objekt zerlegen bzw. object deconstruction
		const { email } = newUser;

		// findeOne erlaubt es uns ein Dokument anhand der im mitgegebenen
		// Objekt identifizierten Merkmale zu lesen.
		const userExists = await User.findOne({ email });

		// existiert der User bereits, speichern wir nicht!
		if (userExists) {
			return res.status(409).json({ message: "User already exists" });
		}

		// Wir speichern den neuen User in der DB
		const savedUser = await newUser.save();
		res.status(201).json(savedUser);
	} catch (error) {
		console.log(error);
		// Confilict status, duplicate
		res.status(409).json({ message: error.message });
	}
};

/**
 * Einen bestehenden User mit eine User Id aktualisieren.
 * Die benötigten Felder sind im userModel definiert.
 * @param Object req
 * @param Object res
 */
export const update = async (req, res) => {
	try {
		const { id } = req.params;

		// existiert User bereits?
		const userExists = await User.findOne({ _id: id });
		if (userExists === null) {
			return res.status(409).json({ message: "User does not exist" });
		}

		// User in DB aktualisieren
		const updatedUser = await User.findByIdAndUpdate(id, req.body, {
			new: true
		});

		res.status(201).json(updatedUser);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
};

/**
 * Einen durch die Id definierten User löschen.
 * @param Object req
 * @param Object res
 */
export const deleteUser = async (req, res) => {
	const id = req.params.id;
	try {
		const { id } = req.params;

		// existiert User bereits?
		const userExists = await User.findOne({ _id: id });
		if (userExists === null) {
			return res.status(409).json({ message: "User does not exist" });
		}

		// User in DB aktualisieren
		const deletedUser = await User.findByIdAndDelete(id, req.body, {
			new: true
		});

		res.status(201).json(deletedUser);
	} catch (error) {
		res.status(409).json({ message: error.message });
	}
};
